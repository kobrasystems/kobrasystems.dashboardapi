﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KobraSystems.DashboardApi.Db
{
    public class Account
    {
        public int AccountId { get; set; }
        public string EmailAddress { get; set; } = string.Empty;
        public string PasswordDigest { get; set; } = string.Empty;
        public bool EmailIsConfirmed { get; set; }
        public string FirstName { get; set; } = string.Empty;
        public string LastName { get; set; } = string.Empty;
        public string PhoneNumber { get; set; } = string.Empty;
        public string Address1 { get; set; } = string.Empty;
        public string Address2 { get; set; } = string.Empty;

        public string City { get; set; } = string.Empty;
        public string State { get; set; } = string.Empty;
        public string CountryCode { get; set; } = string.Empty;
        public string PostalCode { get; set; } = string.Empty;

        public string CompanyName { get; set; } = string.Empty;

        public string StripeCustomerId { get; set; } = string.Empty;

        public virtual ICollection<Website> WebSites { get; set; } = new List<Website>();

    }
}
