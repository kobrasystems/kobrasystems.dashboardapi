﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KobraSystems.DashboardApi.Db
{
    public class ServiceGroup
    {
        public int ServiceGroupId { get; set; }
        public virtual WebsiteService WebsiteService { get; set; } = null!;
        public virtual MySqlService MySqlService { get; set; } = null!;
    }
}
