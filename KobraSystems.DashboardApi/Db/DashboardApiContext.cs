﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace KobraSystems.DashboardApi.Db
{
    public class DashboardApiContext : DbContext
    {
        public DashboardApiContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Account> Accounts => Set<Account>();
        public DbSet<Website> Websites => Set<Website>();
        public DbSet<Product> Products => Set<Product>();

        public DbSet<WebsiteService> WebsiteServices => Set<WebsiteService>();
        public DbSet<ServiceGroup> ServiceGroups => Set<ServiceGroup>();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>().Property(s => s.Address1).IsRequired();
            modelBuilder.Entity<Account>().Property(s => s.Address2).IsRequired(false);
            modelBuilder.Entity<Account>().Property(s => s.CountryCode).IsRequired();
            modelBuilder.Entity<Account>().Property(s => s.EmailAddress).IsRequired();
            modelBuilder.Entity<Account>().Property(s => s.FirstName).IsRequired();
            modelBuilder.Entity<Account>().Property(s => s.LastName).IsRequired();
            modelBuilder.Entity<Account>().Property(s => s.PasswordDigest).IsRequired();
            modelBuilder.Entity<Account>().Property(s => s.PhoneNumber).IsRequired();
            modelBuilder.Entity<Account>().Property(s => s.PostalCode).IsRequired();
            modelBuilder.Entity<Account>().Property(s => s.City).IsRequired();
            modelBuilder.Entity<Account>().Property(s => s.CompanyName).IsRequired(false);
            modelBuilder.Entity<Account>().Property(s => s.StripeCustomerId).IsRequired(false);
        }
    }
}
