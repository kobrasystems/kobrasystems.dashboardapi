﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KobraSystems.DashboardApi.Db
{
    public class Product
    {
        public int ProductId { get; set; }
        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;
        public long MonthlyCost { get; set; }
        public long AnnualCost { get; set; }
        public string StripeProductId { get; set; } = string.Empty;
        public string StripePriceId { get; set; } = string.Empty;
        public bool ShownToCustomer { get; set; }

        public virtual ICollection<Website> WebSites { get; set; } = new List<Website>();

        public virtual ServiceGroup ServiceGroup { get; set; } = null!;

        public int DiskLimitInMb { get; set; }

        public int BandwidthInMb { get; set; }

        public int DomainLimit { get; set; }

        public int MySqlDiskLimitInMb { get; set; }
        public int SqlServerDiskLimitInMb { get; set; }

        public int EmailDiskLimitInMb { get; set; }

        public int DomainAliasLimit { get; set; }

        public int SubDomainsLimit { get; set; }
        public int FtpAccountLimit { get; set; }

        public int EmailAccountLimit { get; set; }



    }
}
