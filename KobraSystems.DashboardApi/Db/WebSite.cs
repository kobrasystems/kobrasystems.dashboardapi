﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Services;

namespace KobraSystems.DashboardApi.Db
{
    public class Website
    {
        public int WebsiteId { get; set; }
        public string Name { get; set; } = string.Empty;
        public string IisSiteName { get; set; } = string.Empty;

        public WebsiteStatus Status { get; set; }

        public virtual Account Account { get; set; } = null!;

        public virtual Product Product { get; set; } = null!;

        public virtual WebsiteService? WebsiteService { get; set; } = null!;

        public enum WebsiteStatus
        {
            Unknown,
            Pending_Setup,
            Active,
            Suspended,
            Canceled
        }
    }
}
