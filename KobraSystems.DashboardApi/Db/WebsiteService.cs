﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KobraSystems.DashboardApi.Db
{
    public class WebsiteService
    {
        public int WebsiteServiceId { get; set; }
        public string Name { get; set; } = string.Empty;
        public string SiteDirectory { get; set; } = string.Empty;
        public string WebRootDirectory { get; set; } = string.Empty;
        public string ServiceIpAddress { get; set; } = string.Empty;
        public string IpAddresses { get; set; } = string.Empty;
        public int ServicePort { get; set; }

        [Column(TypeName = "nvarchar(256)")]
        public string Auth { get; set; } = string.Empty;

        public virtual ICollection<Website> WebSites { get; set; } = new List<Website>();

        public string GetWebsiteServiceAddress() => $"https://{ServiceIpAddress}:{ServicePort}/api";

        public string GetWebsitePhysicalPath() => System.IO.Path.Combine(SiteDirectory, WebRootDirectory);

        public string GetWebsiteLogsPath() => System.IO.Path.Combine(SiteDirectory, "logs");


    }
}
