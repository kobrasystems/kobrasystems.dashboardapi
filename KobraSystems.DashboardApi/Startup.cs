using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Configuration;
using KobraSystems.DashboardApi.Db;
using KobraSystems.DashboardApi.Services;
using KobraSystems.DashboardApi.Services.Db;
using KobraSystems.DashboardApi.Services.Stripe;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using WebsiteService = KobraSystems.DashboardApi.Services.WebsiteService;

namespace KobraSystems.DashboardApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private readonly string Allow_All = "allow_all";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(Allow_All, builder =>
                {
                    builder
                        .WithOrigins("http://localhost:3000", "https://dashboard.kobrahosting.com", "http://localhost:4200")
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            services.AddControllers();
            services.AddDbContext<DashboardApiContext>(item =>
                item.UseSqlServer(Configuration.GetConnectionString("DashboardApi")));

            services.AddSingleton<AuthService>();
            services.AddScoped<AccountService>();
            services.AddScoped<ProductService>();
            services.AddScoped<StripeCheckoutService>();
            services.AddScoped<StripeCustomerService>();
            services.AddSingleton<WebsiteService>();
            services.AddSingleton<WebsiteCreationService>();
            services.AddScoped<Services.Db.WebsiteService>();

            services.Configure<StripeOptions>(Configuration.GetSection("StripeOptions"));

            string jwtKey = Configuration.GetValue<string>("JwtKey");

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = AuthService.GetTokenValidationParameters(jwtKey);
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "KobraSystems.DashboardApi", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "KobraSystems.DashboardApi v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(Allow_All);

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
