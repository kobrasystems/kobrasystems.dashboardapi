﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Configuration;
using Microsoft.Extensions.Options;
using Stripe;
using Stripe.Checkout;
using Product = KobraSystems.DashboardApi.Db.Product;

namespace KobraSystems.DashboardApi.Services.Stripe
{
    public class StripeCheckoutService
    {
        private readonly StripeOptions _stripeOptions;

        public StripeCheckoutService(IOptions<StripeOptions> stripeOptions)
        {
            _stripeOptions = stripeOptions.Value;
        }

        public Session GetCheckoutSession(string sessionId)
        {
            StripeConfiguration.ApiKey = _stripeOptions.SecretKey;

            var service = new SessionService();
            return service.Get(sessionId);
        }

        public StripeList<LineItem> GetSessionLineItems(string sessionId)
        {
            StripeConfiguration.ApiKey = _stripeOptions.SecretKey;

            var options = new SessionListLineItemsOptions
            {
                Limit = 1000
            };

            var service = new SessionService();
            StripeList<LineItem> lineItems = service.ListLineItems(sessionId, options);

            return lineItems;
        }

        public Session CreateCheckoutSession(int accountId, string emailAddress, string customerId, string webSiteName, Product product)
        {
            var options = new SessionCreateOptions
            {
                Customer = customerId,
                CancelUrl = _stripeOptions.ReturnUrl,
                Mode = "subscription",
                PaymentMethodTypes = new List<string>() { "card" },
                SuccessUrl = _stripeOptions.SuccessUrl,
                Metadata = new Dictionary<string, string>()
                {
                    { "AccountId", accountId.ToString() },
                    { "Name",  webSiteName }
                },
                LineItems = new List<SessionLineItemOptions>()
                 {
                     new SessionLineItemOptions()
                     {
                         Description = product.Description,
                         Price = product.StripePriceId,
                         Quantity = 1
                     }
                 }
            };

            var service = new SessionService();
            return service.Create(options);
        }
    }
}
