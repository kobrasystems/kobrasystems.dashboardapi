﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Configuration;
using KobraSystems.DashboardApi.Db;
using Microsoft.Extensions.Options;
using Stripe;

namespace KobraSystems.DashboardApi.Services.Stripe
{
    public class StripeCustomerService
    {
        private DashboardApiContext _db;
        private StripeOptions _stripeOptions;

        public StripeCustomerService(DashboardApiContext context, IOptions<StripeOptions> stripeOptions)
        {
            _db = context;
            _stripeOptions = stripeOptions.Value;
        }

        public Customer CreateCustomer(string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(emailAddress));

            StripeConfiguration.ApiKey = _stripeOptions.SecretKey;

            var customerOptions = new CustomerCreateOptions
            {
                Description = $"Customer for {emailAddress}",
            };

            // create stripe customer
            CustomerService customerService = new CustomerService();
            Customer customer = customerService.Create(customerOptions);

            return customer;
        }
    }
}
