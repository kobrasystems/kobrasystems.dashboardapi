﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Db;
using KobraSystems.DashboardRemoteApi.Models.Request;

namespace KobraSystems.DashboardApi.Services
{
    public class WebsiteCreationService
    {
        private readonly WebsiteService _websiteService;

        public WebsiteCreationService(WebsiteService websiteService)
        {
            _websiteService = websiteService;
        }

        public async Task CreateWebsiteAsync(DashboardApi.Db.WebsiteService websiteService, Website website)
        {
            var remoteService = new RemoteService(websiteService);

            await remoteService.CreateDirectoryAsync(new CreateDirectoryRequest
            {
                Path = websiteService.GetWebsitePhysicalPath(),
            }, websiteService);

            await remoteService.CreateDirectoryAsync(new CreateDirectoryRequest
            {
                Path = websiteService.GetWebsiteLogsPath()
            }, websiteService);

            await _websiteService.CreateWebsiteAsync(websiteService, website);
            await _websiteService.CreateApplicationPoolAsync(websiteService, website);
            await _websiteService.SetAnonymousAuthenticationAsync(websiteService, website, website.Name.ToLowerInvariant());

            var site = await _websiteService.GetSiteAsync(websiteService, website);

            await remoteService.CreateDirectoryAsync(new CreateDirectoryRequest
            {
                Path = System.IO.Path.Combine(websiteService.GetWebsiteLogsPath(), $"W3SVC{site.Id}")
            }, websiteService);

        }
    }
}
