﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Db;
using Microsoft.EntityFrameworkCore;

namespace KobraSystems.DashboardApi.Services.Db
{
    public class ProductService
    {
        private DashboardApiContext _db;

        public ProductService(DashboardApiContext context)
        {
            _db = context;
        }

        public Product[] GetProducts()
        {
            return _db.Products.ToArray();
        }

        public async Task<Product?> GetProductById(int productId)
        {
            if (productId <= 0) throw new ArgumentOutOfRangeException(nameof(productId));

            var product = await _db.Products.SingleOrDefaultAsync(p => p.ProductId == productId);

            return product;
        }
    }
}
