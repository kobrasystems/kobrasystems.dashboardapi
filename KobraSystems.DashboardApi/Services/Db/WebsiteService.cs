﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Db;

namespace KobraSystems.DashboardApi.Services.Db
{
    public class WebsiteService
    {
        private readonly DashboardApiContext _db;

        public WebsiteService(DashboardApiContext context)
        {
            _db = context;
        }

        public async Task UpdateWebsiteAsync(int websiteId, string websiteName, Website.WebsiteStatus websiteStatus)
        {
            var website = _db.Websites.SingleOrDefault(w => w.WebsiteId == websiteId)
                ?? throw new Exception("Invalid website");

            website.IisSiteName = websiteName;
            website.Status = websiteStatus;

            await _db.SaveChangesAsync();
        }

        public async Task<Website> CreateWebsite(Account account, Product product, DashboardApi.Db.WebsiteService websiteService, string websiteName, Website.WebsiteStatus websiteStatus)
        {
            var website = new Website
            {
                Account = account,
                WebsiteService = websiteService,
                IisSiteName = websiteName.ToLowerInvariant(),
                Name = websiteName.ToLowerInvariant(),
                Status = websiteStatus,
                Product = product,
            };

            await _db.Websites.AddAsync(website);
            await _db.SaveChangesAsync();

            return website;
        }
    }
}
