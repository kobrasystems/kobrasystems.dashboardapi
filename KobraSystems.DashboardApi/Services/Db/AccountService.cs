﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Db;
using Microsoft.EntityFrameworkCore;

namespace KobraSystems.DashboardApi.Services.Db
{
    public class AccountService
    {
        private readonly DashboardApiContext _db;

        public AccountService(DashboardApiContext context)
        {
            _db = context;
        }

        public async Task<Account> CreateAccountAsync(string firstName, string lastName, string companyName, string address1, string address2, string city, string countryCode, string emailAddress, string phoneNumber, string passwordDigest, string postalCode, string state)
        {
            if (string.IsNullOrWhiteSpace(firstName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(firstName));
            if (string.IsNullOrWhiteSpace(lastName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(lastName));
            if (string.IsNullOrWhiteSpace(address1))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(address1));
            if (string.IsNullOrWhiteSpace(city))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(city));
            if (string.IsNullOrWhiteSpace(countryCode))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(countryCode));
            if (string.IsNullOrWhiteSpace(emailAddress))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(emailAddress));
            if (string.IsNullOrWhiteSpace(phoneNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(phoneNumber));
            if (string.IsNullOrWhiteSpace(passwordDigest))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(passwordDigest));
            if (string.IsNullOrWhiteSpace(postalCode))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(postalCode));
            if (string.IsNullOrWhiteSpace(state))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(state));

            var account = new Account
            {
                CompanyName = companyName,
                Address1 = address1,
                Address2 = address2,
                City = city,
                CountryCode = countryCode,
                EmailAddress = emailAddress,
                FirstName = firstName,
                LastName = lastName,
                PhoneNumber = phoneNumber,
                PasswordDigest = passwordDigest,
                PostalCode = postalCode,
                State = state
            };

            await _db.Accounts.AddAsync(account);
            await _db.SaveChangesAsync();

            return account;
        }

        public async Task<Account?> GetAccountByIdAsync(int accountId)
        {
            var account = await _db.Accounts.SingleOrDefaultAsync(a => a.AccountId == accountId);

            return account;
        }
    }
}
