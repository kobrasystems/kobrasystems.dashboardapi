﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Db;
using KobraSystems.DashboardRemoteApi.Models.Request;
using KobraSystems.DashboardRemoteApi.Models.Response;
using Newtonsoft.Json;

namespace KobraSystems.DashboardApi.Services
{
    public class RemoteService
    {
        public static Dictionary<DashboardApi.Db.WebsiteService, HttpClient> RemoteClients = new Dictionary<DashboardApi.Db.WebsiteService, HttpClient>();

        private HttpClient _client;

        public RemoteService(DashboardApi.Db.WebsiteService server)
        {
            if (RemoteClients.TryGetValue(server, out HttpClient? client))
            {
                _client = client;
            }
            else
            {
                var newClient = new HttpClient();
                RemoteClients.Add(server, newClient);
                _client = newClient;
            }
        }

        public async Task CreateSiteAsync(CreateSiteRequest model, Website website)
        {
            if (website.WebsiteService == null)
            {
                throw new Exception("Cannot create IIS site when website does not have a WebsiteService");
            }

            string url = website.WebsiteService.GetWebsiteServiceAddress() + "/Sites/CreateSite";
            await postNoBodyAsync<CreateSiteRequest, object>(model, url);
        }

        public async Task CreateApplicationPoolAsync(CreateApplicationPoolRequest model, Website website)
        {
            if (website.WebsiteService == null)
            {
                throw new Exception("Cannot create application pool when website does not have a WebsiteService");
            }

            string url = website.WebsiteService.GetWebsiteServiceAddress() + "/ApplicationPools/CreateApplicationPool";
            await postNoBodyAsync<CreateApplicationPoolRequest, object>(model, url);
        }

        public async Task CreateDirectoryAsync(CreateDirectoryRequest model, KobraSystems.DashboardApi.Db.WebsiteService websiteService)
        {
            string url = websiteService.GetWebsiteServiceAddress() + "/Directory/CreateDirectory";
            await postNoBodyAsync<CreateDirectoryRequest, object>(model, url);
        }

        public async Task SetAnonymousAuthentication(SetAnonymousAuthenticationRequest model, Website website)
        {
            if (website.WebsiteService == null)
            {
                throw new Exception("Cannot set anonymous authentication when website does not have a WebsiteService");
            }

            string url = website.WebsiteService.GetWebsiteServiceAddress() +
                         "/Authentication/SetAnonymousAuthentication";
            await postNoBodyAsync<SetAnonymousAuthenticationRequest, object>(model, url);
        }

        public async Task<GetSiteResponse> GetSiteAsync(Website website)
        {
            if (website.WebsiteService == null)
            {
                throw new Exception("Cannot get IIS Site when website does not have a WebsiteService");
            }

            string url = website.WebsiteService.GetWebsiteServiceAddress() + "/Sites/GetSite";
            var site = await postAsync<GetSiteRequest, GetSiteResponse>(new GetSiteRequest
            {
                IisSiteName = website.IisSiteName
            }, url);

            return site;
        }

        private async Task<TResult> postAsync<T, TResult>(T postBody, string url)
        {
            HttpRequestMessage request = new(HttpMethod.Post, url)
            {
                Content = new StringContent(JsonConvert.SerializeObject(postBody))
            };

            var response = await _client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var contentStr = await response.Content.ReadAsStringAsync();
                var remoteApiResponse = JsonConvert.DeserializeObject<RemoteApiResponse<TResult>>(contentStr);

                if (remoteApiResponse.Errors.Any())
                {
                    // TODO: log errors
                    throw new Exception("An unexpected error occurred");
                }

                if (remoteApiResponse.Body == null)
                {
                    // TODO: log error
                    throw new Exception("An unexpected error occurred");
                }

                return remoteApiResponse.Body;
            }
            else
            {
                throw new Exception($"Encountered non-200 OK response: {(int)response.StatusCode} {response.StatusCode}");
            }
        }

        private async Task postNoBodyAsync<T, TResult>(T postBody, string url)
        {
            HttpRequestMessage request = new(HttpMethod.Post, url)
            {
                Content = new StringContent(JsonConvert.SerializeObject(postBody))
            };

            var response = await _client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Encountered non-200 OK response: {(int)response.StatusCode} {response.StatusCode}");
            }

            var contentStr = await response.Content.ReadAsStringAsync();
            var responseObj = JsonConvert.DeserializeObject<RemoteApiResponse<TResult>>(contentStr);

            if (responseObj.Errors.Any())
            {
                // TODO: Log to something
                throw new Exception("An unexpected error occurred");
            }
        }
    }
}
