﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Db;
using KobraSystems.DashboardRemoteApi.Models.Request;
using KobraSystems.DashboardRemoteApi.Models.Response;
using Microsoft.Web.Administration;

namespace KobraSystems.DashboardApi.Services
{
    public class WebsiteService
    {
        public async Task<GetSiteResponse> GetSiteAsync(DashboardApi.Db.WebsiteService websiteService, Website website)
        {
            var remoteService = new RemoteService(websiteService);

            var site = await remoteService.GetSiteAsync(website);

            return site;
        }

        public async Task CreateWebsiteAsync(DashboardApi.Db.WebsiteService websiteService, Website website)
        {
            var formattedSiteName = website.Name.ToLowerInvariant();

            var remoteService = new RemoteService(websiteService);
            await remoteService.CreateSiteAsync(new CreateSiteRequest
            {
                SiteName = formattedSiteName,
                ApplicationPool = formattedSiteName,
                BindingProtocol = "HTTP",
                Bindings = new[] { $"80:{formattedSiteName}", $"80:www.{formattedSiteName}" },
                LogDirectory = websiteService.GetWebsiteLogsPath(),
                PhysicalPath = websiteService.GetWebsitePhysicalPath()
            }, website);
        }

        public async Task CreateApplicationPoolAsync(DashboardApi.Db.WebsiteService websiteService, Website website)
        {
            var remoteService = new RemoteService(websiteService);
            await remoteService.CreateApplicationPoolAsync(new CreateApplicationPoolRequest
            {
                ApplicationPoolName = website.Name.ToLowerInvariant(),
                Enable32BitAppOnWin64 = false,
                ManagedPipelineMode = ManagedPipelineMode.Integrated,
                ProcessModelIdentityType = ProcessModelIdentityType.ApplicationPoolIdentity
            }, website);
        }

        public async Task SetAnonymousAuthenticationAsync(DashboardApi.Db.WebsiteService websiteService, Website website, string iisSiteName)
        {
            var remoteService = new RemoteService(websiteService);
            await remoteService.SetAnonymousAuthentication(new SetAnonymousAuthenticationRequest
            {
                Enabled = true,
                SiteId = iisSiteName
            }, website);
        }
    }
}
