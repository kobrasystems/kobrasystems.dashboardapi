﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KobraSystems.DashboardApi.Configuration
{
    public class StripeOptions
    {
        public string SecretKey { get; set; } = string.Empty;
        public string PublishableKey { get; set; } = string.Empty;
        public string WebhookSecret { get; set; } = string.Empty;
        public string ReturnUrl { get; set; } = string.Empty;
        public string SuccessUrl { get; set; } = string.Empty;

        public string StripeLogFile { get; set; } = string.Empty;
    }
}
