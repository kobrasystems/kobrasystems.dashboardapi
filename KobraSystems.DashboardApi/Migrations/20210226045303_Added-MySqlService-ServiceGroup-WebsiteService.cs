﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KobraSystems.DashboardApi.Migrations
{
    public partial class AddedMySqlServiceServiceGroupWebsiteService : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WebSites_Accounts_AccountId",
                table: "WebSites");

            migrationBuilder.DropForeignKey(
                name: "FK_WebSites_Products_ProductId",
                table: "WebSites");

            migrationBuilder.DropForeignKey(
                name: "FK_WebSites_Server_ServerId",
                table: "WebSites");

            migrationBuilder.DropTable(
                name: "Server");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WebSites",
                table: "WebSites");

            migrationBuilder.RenameTable(
                name: "WebSites",
                newName: "Websites");

            migrationBuilder.RenameColumn(
                name: "WebSiteId",
                table: "Websites",
                newName: "WebsiteId");

            migrationBuilder.RenameColumn(
                name: "WebSiteName",
                table: "Websites",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "ServerId",
                table: "Websites",
                newName: "WebsiteServiceId");

            migrationBuilder.RenameIndex(
                name: "IX_WebSites_ProductId",
                table: "Websites",
                newName: "IX_Websites_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_WebSites_AccountId",
                table: "Websites",
                newName: "IX_Websites_AccountId");

            migrationBuilder.RenameIndex(
                name: "IX_WebSites_ServerId",
                table: "Websites",
                newName: "IX_Websites_WebsiteServiceId");

            migrationBuilder.AddColumn<int>(
                name: "ServiceGroupId",
                table: "Products",
                type: "int",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Websites",
                table: "Websites",
                column: "WebsiteId");

            migrationBuilder.CreateTable(
                name: "MySqlService",
                columns: table => new
                {
                    MySqlServiceId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IpAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MySqlService", x => x.MySqlServiceId);
                });

            migrationBuilder.CreateTable(
                name: "WebsiteService",
                columns: table => new
                {
                    WebsiteServiceId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SiteDirectory = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    WebRootDirectory = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ServiceIpAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IpAddresses = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ServicePort = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebsiteService", x => x.WebsiteServiceId);
                });

            migrationBuilder.CreateTable(
                name: "ServiceGroup",
                columns: table => new
                {
                    ServiceGroupId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WebsiteServiceId = table.Column<int>(type: "int", nullable: true),
                    MySqlServiceId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceGroup", x => x.ServiceGroupId);
                    table.ForeignKey(
                        name: "FK_ServiceGroup_MySqlService_MySqlServiceId",
                        column: x => x.MySqlServiceId,
                        principalTable: "MySqlService",
                        principalColumn: "MySqlServiceId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceGroup_WebsiteService_WebsiteServiceId",
                        column: x => x.WebsiteServiceId,
                        principalTable: "WebsiteService",
                        principalColumn: "WebsiteServiceId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_ServiceGroupId",
                table: "Products",
                column: "ServiceGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceGroup_MySqlServiceId",
                table: "ServiceGroup",
                column: "MySqlServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceGroup_WebsiteServiceId",
                table: "ServiceGroup",
                column: "WebsiteServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_ServiceGroup_ServiceGroupId",
                table: "Products",
                column: "ServiceGroupId",
                principalTable: "ServiceGroup",
                principalColumn: "ServiceGroupId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Websites_Accounts_AccountId",
                table: "Websites",
                column: "AccountId",
                principalTable: "Accounts",
                principalColumn: "AccountId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Websites_Products_ProductId",
                table: "Websites",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "ProductId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Websites_WebsiteService_WebsiteServiceId",
                table: "Websites",
                column: "WebsiteServiceId",
                principalTable: "WebsiteService",
                principalColumn: "WebsiteServiceId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_ServiceGroup_ServiceGroupId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_Websites_Accounts_AccountId",
                table: "Websites");

            migrationBuilder.DropForeignKey(
                name: "FK_Websites_Products_ProductId",
                table: "Websites");

            migrationBuilder.DropForeignKey(
                name: "FK_Websites_WebsiteService_WebsiteServiceId",
                table: "Websites");

            migrationBuilder.DropTable(
                name: "ServiceGroup");

            migrationBuilder.DropTable(
                name: "MySqlService");

            migrationBuilder.DropTable(
                name: "WebsiteService");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Websites",
                table: "Websites");

            migrationBuilder.DropIndex(
                name: "IX_Products_ServiceGroupId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "ServiceGroupId",
                table: "Products");

            migrationBuilder.RenameTable(
                name: "Websites",
                newName: "WebSites");

            migrationBuilder.RenameColumn(
                name: "WebsiteId",
                table: "WebSites",
                newName: "WebSiteId");

            migrationBuilder.RenameColumn(
                name: "WebsiteServiceId",
                table: "WebSites",
                newName: "ServerId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "WebSites",
                newName: "WebSiteName");

            migrationBuilder.RenameIndex(
                name: "IX_Websites_ProductId",
                table: "WebSites",
                newName: "IX_WebSites_ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_Websites_AccountId",
                table: "WebSites",
                newName: "IX_WebSites_AccountId");

            migrationBuilder.RenameIndex(
                name: "IX_Websites_WebsiteServiceId",
                table: "WebSites",
                newName: "IX_WebSites_ServerId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WebSites",
                table: "WebSites",
                column: "WebSiteId");

            migrationBuilder.CreateTable(
                name: "Server",
                columns: table => new
                {
                    ServerId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IpAddresses = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ServiceIpAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ServicePort = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Server", x => x.ServerId);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_WebSites_Accounts_AccountId",
                table: "WebSites",
                column: "AccountId",
                principalTable: "Accounts",
                principalColumn: "AccountId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WebSites_Products_ProductId",
                table: "WebSites",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "ProductId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WebSites_Server_ServerId",
                table: "WebSites",
                column: "ServerId",
                principalTable: "Server",
                principalColumn: "ServerId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
