﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KobraSystems.DashboardApi.Migrations
{
    public partial class AddedPackageLimitsToProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BandwidthInMb",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DiskLimitInMb",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DomainAliasLimit",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DomainLimit",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EmailAccountLimit",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EmailDiskLimitInMb",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "FtpAccountLimit",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MySqlDiskLimitInMb",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SqlServerDiskLimitInMb",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SubDomainsLimit",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BandwidthInMb",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "DiskLimitInMb",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "DomainAliasLimit",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "DomainLimit",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "EmailAccountLimit",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "EmailDiskLimitInMb",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "FtpAccountLimit",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "MySqlDiskLimitInMb",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "SqlServerDiskLimitInMb",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "SubDomainsLimit",
                table: "Products");
        }
    }
}
