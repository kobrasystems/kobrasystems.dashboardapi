﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KobraSystems.DashboardApi.Migrations
{
    public partial class AddedServerIdToServerTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ServerId",
                table: "WebSites",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Server",
                columns: table => new
                {
                    ServerId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ServiceIpAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IpAddresses = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ServicePort = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Server", x => x.ServerId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WebSites_ServerId",
                table: "WebSites",
                column: "ServerId");

            migrationBuilder.AddForeignKey(
                name: "FK_WebSites_Server_ServerId",
                table: "WebSites",
                column: "ServerId",
                principalTable: "Server",
                principalColumn: "ServerId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WebSites_Server_ServerId",
                table: "WebSites");

            migrationBuilder.DropTable(
                name: "Server");

            migrationBuilder.DropIndex(
                name: "IX_WebSites_ServerId",
                table: "WebSites");

            migrationBuilder.DropColumn(
                name: "ServerId",
                table: "WebSites");
        }
    }
}
