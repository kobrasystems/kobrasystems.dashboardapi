﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KobraSystems.DashboardApi.Migrations
{
    public partial class AddedAuthToWebsiteService : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceGroup_WebsiteService_WebsiteServiceId",
                table: "ServiceGroup");

            migrationBuilder.DropForeignKey(
                name: "FK_Websites_WebsiteService_WebsiteServiceId",
                table: "Websites");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WebsiteService",
                table: "WebsiteService");

            migrationBuilder.RenameTable(
                name: "WebsiteService",
                newName: "WebsiteServices");

            migrationBuilder.AddColumn<string>(
                name: "Auth",
                table: "WebsiteServices",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WebsiteServices",
                table: "WebsiteServices",
                column: "WebsiteServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceGroup_WebsiteServices_WebsiteServiceId",
                table: "ServiceGroup",
                column: "WebsiteServiceId",
                principalTable: "WebsiteServices",
                principalColumn: "WebsiteServiceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Websites_WebsiteServices_WebsiteServiceId",
                table: "Websites",
                column: "WebsiteServiceId",
                principalTable: "WebsiteServices",
                principalColumn: "WebsiteServiceId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceGroup_WebsiteServices_WebsiteServiceId",
                table: "ServiceGroup");

            migrationBuilder.DropForeignKey(
                name: "FK_Websites_WebsiteServices_WebsiteServiceId",
                table: "Websites");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WebsiteServices",
                table: "WebsiteServices");

            migrationBuilder.DropColumn(
                name: "Auth",
                table: "WebsiteServices");

            migrationBuilder.RenameTable(
                name: "WebsiteServices",
                newName: "WebsiteService");

            migrationBuilder.AddPrimaryKey(
                name: "PK_WebsiteService",
                table: "WebsiteService",
                column: "WebsiteServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceGroup_WebsiteService_WebsiteServiceId",
                table: "ServiceGroup",
                column: "WebsiteServiceId",
                principalTable: "WebsiteService",
                principalColumn: "WebsiteServiceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Websites_WebsiteService_WebsiteServiceId",
                table: "Websites",
                column: "WebsiteServiceId",
                principalTable: "WebsiteService",
                principalColumn: "WebsiteServiceId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
