﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KobraSystems.DashboardApi.Migrations
{
    public partial class AddedAuthColumn3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_ServiceGroups_ServiceGroupId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceGroups_MySqlService_MySqlServiceId",
                table: "ServiceGroups");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ServiceGroups",
                table: "ServiceGroups");

            migrationBuilder.RenameTable(
                name: "ServiceGroups",
                newName: "ServiceGroup");

            migrationBuilder.RenameIndex(
                name: "IX_ServiceGroups_MySqlServiceId",
                table: "ServiceGroup",
                newName: "IX_ServiceGroup_MySqlServiceId");

            migrationBuilder.AddColumn<int>(
                name: "WebsiteServiceId",
                table: "Websites",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WebsiteServiceId",
                table: "ServiceGroup",
                type: "int",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ServiceGroup",
                table: "ServiceGroup",
                column: "ServiceGroupId");

            migrationBuilder.CreateTable(
                name: "WebsiteService",
                columns: table => new
                {
                    WebsiteServiceId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SiteDirectory = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    WebRootDirectory = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ServiceIpAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IpAddresses = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ServicePort = table.Column<int>(type: "int", nullable: false),
                    Auth = table.Column<byte[]>(type: "varbinary(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebsiteService", x => x.WebsiteServiceId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Websites_WebsiteServiceId",
                table: "Websites",
                column: "WebsiteServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceGroup_WebsiteServiceId",
                table: "ServiceGroup",
                column: "WebsiteServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_ServiceGroup_ServiceGroupId",
                table: "Products",
                column: "ServiceGroupId",
                principalTable: "ServiceGroup",
                principalColumn: "ServiceGroupId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceGroup_MySqlService_MySqlServiceId",
                table: "ServiceGroup",
                column: "MySqlServiceId",
                principalTable: "MySqlService",
                principalColumn: "MySqlServiceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceGroup_WebsiteService_WebsiteServiceId",
                table: "ServiceGroup",
                column: "WebsiteServiceId",
                principalTable: "WebsiteService",
                principalColumn: "WebsiteServiceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Websites_WebsiteService_WebsiteServiceId",
                table: "Websites",
                column: "WebsiteServiceId",
                principalTable: "WebsiteService",
                principalColumn: "WebsiteServiceId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_ServiceGroup_ServiceGroupId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceGroup_MySqlService_MySqlServiceId",
                table: "ServiceGroup");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceGroup_WebsiteService_WebsiteServiceId",
                table: "ServiceGroup");

            migrationBuilder.DropForeignKey(
                name: "FK_Websites_WebsiteService_WebsiteServiceId",
                table: "Websites");

            migrationBuilder.DropTable(
                name: "WebsiteService");

            migrationBuilder.DropIndex(
                name: "IX_Websites_WebsiteServiceId",
                table: "Websites");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ServiceGroup",
                table: "ServiceGroup");

            migrationBuilder.DropIndex(
                name: "IX_ServiceGroup_WebsiteServiceId",
                table: "ServiceGroup");

            migrationBuilder.DropColumn(
                name: "WebsiteServiceId",
                table: "Websites");

            migrationBuilder.DropColumn(
                name: "WebsiteServiceId",
                table: "ServiceGroup");

            migrationBuilder.RenameTable(
                name: "ServiceGroup",
                newName: "ServiceGroups");

            migrationBuilder.RenameIndex(
                name: "IX_ServiceGroup_MySqlServiceId",
                table: "ServiceGroups",
                newName: "IX_ServiceGroups_MySqlServiceId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ServiceGroups",
                table: "ServiceGroups",
                column: "ServiceGroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_ServiceGroups_ServiceGroupId",
                table: "Products",
                column: "ServiceGroupId",
                principalTable: "ServiceGroups",
                principalColumn: "ServiceGroupId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceGroups_MySqlService_MySqlServiceId",
                table: "ServiceGroups",
                column: "MySqlServiceId",
                principalTable: "MySqlService",
                principalColumn: "MySqlServiceId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
