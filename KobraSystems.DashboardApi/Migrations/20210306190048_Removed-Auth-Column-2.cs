﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KobraSystems.DashboardApi.Migrations
{
    public partial class RemovedAuthColumn2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_ServiceGroup_ServiceGroupId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceGroup_MySqlService_MySqlServiceId",
                table: "ServiceGroup");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceGroup_WebsiteServices_WebsiteServiceId",
                table: "ServiceGroup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ServiceGroup",
                table: "ServiceGroup");

            //migrationBuilder.DropColumn(
            //    name: "Auth",
            //    table: "WebsiteServices");

            migrationBuilder.RenameTable(
                name: "ServiceGroup",
                newName: "ServiceGroups");

            migrationBuilder.RenameIndex(
                name: "IX_ServiceGroup_WebsiteServiceId",
                table: "ServiceGroups",
                newName: "IX_ServiceGroups_WebsiteServiceId");

            migrationBuilder.RenameIndex(
                name: "IX_ServiceGroup_MySqlServiceId",
                table: "ServiceGroups",
                newName: "IX_ServiceGroups_MySqlServiceId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ServiceGroups",
                table: "ServiceGroups",
                column: "ServiceGroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_ServiceGroups_ServiceGroupId",
                table: "Products",
                column: "ServiceGroupId",
                principalTable: "ServiceGroups",
                principalColumn: "ServiceGroupId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceGroups_MySqlService_MySqlServiceId",
                table: "ServiceGroups",
                column: "MySqlServiceId",
                principalTable: "MySqlService",
                principalColumn: "MySqlServiceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceGroups_WebsiteServices_WebsiteServiceId",
                table: "ServiceGroups",
                column: "WebsiteServiceId",
                principalTable: "WebsiteServices",
                principalColumn: "WebsiteServiceId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_ServiceGroups_ServiceGroupId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceGroups_MySqlService_MySqlServiceId",
                table: "ServiceGroups");

            migrationBuilder.DropForeignKey(
                name: "FK_ServiceGroups_WebsiteServices_WebsiteServiceId",
                table: "ServiceGroups");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ServiceGroups",
                table: "ServiceGroups");

            migrationBuilder.RenameTable(
                name: "ServiceGroups",
                newName: "ServiceGroup");

            migrationBuilder.RenameIndex(
                name: "IX_ServiceGroups_WebsiteServiceId",
                table: "ServiceGroup",
                newName: "IX_ServiceGroup_WebsiteServiceId");

            migrationBuilder.RenameIndex(
                name: "IX_ServiceGroups_MySqlServiceId",
                table: "ServiceGroup",
                newName: "IX_ServiceGroup_MySqlServiceId");



            migrationBuilder.AddPrimaryKey(
                name: "PK_ServiceGroup",
                table: "ServiceGroup",
                column: "ServiceGroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_ServiceGroup_ServiceGroupId",
                table: "Products",
                column: "ServiceGroupId",
                principalTable: "ServiceGroup",
                principalColumn: "ServiceGroupId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceGroup_MySqlService_MySqlServiceId",
                table: "ServiceGroup",
                column: "MySqlServiceId",
                principalTable: "MySqlService",
                principalColumn: "MySqlServiceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceGroup_WebsiteServices_WebsiteServiceId",
                table: "ServiceGroup",
                column: "WebsiteServiceId",
                principalTable: "WebsiteServices",
                principalColumn: "WebsiteServiceId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
