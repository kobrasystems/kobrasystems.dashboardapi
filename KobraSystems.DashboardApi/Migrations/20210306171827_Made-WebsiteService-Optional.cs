﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KobraSystems.DashboardApi.Migrations
{
    public partial class MadeWebsiteServiceOptional : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Websites_WebsiteService_WebsiteServiceId",
                table: "Websites");

            migrationBuilder.AlterColumn<int>(
                name: "WebsiteServiceId",
                table: "Websites",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Websites_WebsiteService_WebsiteServiceId",
                table: "Websites",
                column: "WebsiteServiceId",
                principalTable: "WebsiteService",
                principalColumn: "WebsiteServiceId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Websites_WebsiteService_WebsiteServiceId",
                table: "Websites");

            migrationBuilder.AlterColumn<int>(
                name: "WebsiteServiceId",
                table: "Websites",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Websites_WebsiteService_WebsiteServiceId",
                table: "Websites",
                column: "WebsiteServiceId",
                principalTable: "WebsiteService",
                principalColumn: "WebsiteServiceId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
