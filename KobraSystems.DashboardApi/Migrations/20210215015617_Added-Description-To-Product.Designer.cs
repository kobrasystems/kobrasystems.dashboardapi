﻿// <auto-generated />
using System;
using KobraSystems.DashboardApi.Db;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace KobraSystems.DashboardApi.Migrations
{
    [DbContext(typeof(DashboardApiContext))]
    [Migration("20210215015617_Added-Description-To-Product")]
    partial class AddedDescriptionToProduct
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.3")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("KobraSystems.DashboardApi.Db.Account", b =>
                {
                    b.Property<int>("AccountId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address1")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Address2")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("City")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("CompanyName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("CountryCode")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("EmailAddress")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("EmailIsConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PasswordDigest")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PostalCode")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("State")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("StripeCustomerId")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("AccountId");

                    b.ToTable("Accounts");
                });

            modelBuilder.Entity("KobraSystems.DashboardApi.Db.Product", b =>
                {
                    b.Property<int>("ProductId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<long>("AnnualCost")
                        .HasColumnType("bigint");

                    b.Property<int>("BandwidthInMb")
                        .HasColumnType("int");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("DiskLimitInMb")
                        .HasColumnType("int");

                    b.Property<int>("DomainAliasLimit")
                        .HasColumnType("int");

                    b.Property<int>("DomainLimit")
                        .HasColumnType("int");

                    b.Property<int>("EmailAccountLimit")
                        .HasColumnType("int");

                    b.Property<int>("EmailDiskLimitInMb")
                        .HasColumnType("int");

                    b.Property<int>("FtpAccountLimit")
                        .HasColumnType("int");

                    b.Property<long>("MonthlyCost")
                        .HasColumnType("bigint");

                    b.Property<int>("MySqlDiskLimitInMb")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("ShownToCustomer")
                        .HasColumnType("bit");

                    b.Property<int>("SqlServerDiskLimitInMb")
                        .HasColumnType("int");

                    b.Property<string>("StripePriceId")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("StripeProductId")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("SubDomainsLimit")
                        .HasColumnType("int");

                    b.HasKey("ProductId");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("KobraSystems.DashboardApi.Db.WebSite", b =>
                {
                    b.Property<int>("WebSiteId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("AccountId")
                        .HasColumnType("int");

                    b.Property<string>("IisSiteName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("ProductId")
                        .HasColumnType("int");

                    b.Property<string>("WebSiteName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("WebSiteId");

                    b.HasIndex("AccountId");

                    b.HasIndex("ProductId");

                    b.ToTable("WebSites");
                });

            modelBuilder.Entity("KobraSystems.DashboardApi.Db.WebSite", b =>
                {
                    b.HasOne("KobraSystems.DashboardApi.Db.Account", null)
                        .WithMany("WebSites")
                        .HasForeignKey("AccountId");

                    b.HasOne("KobraSystems.DashboardApi.Db.Product", "Product")
                        .WithMany("WebSites")
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Product");
                });

            modelBuilder.Entity("KobraSystems.DashboardApi.Db.Account", b =>
                {
                    b.Navigation("WebSites");
                });

            modelBuilder.Entity("KobraSystems.DashboardApi.Db.Product", b =>
                {
                    b.Navigation("WebSites");
                });
#pragma warning restore 612, 618
        }
    }
}
