﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KobraSystems.DashboardApi.Migrations
{
    public partial class RemovedWebsiteServiceTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceGroups_WebsiteServices_WebsiteServiceId",
                table: "ServiceGroups");

            migrationBuilder.DropForeignKey(
                name: "FK_Websites_WebsiteServices_WebsiteServiceId",
                table: "Websites");

            migrationBuilder.DropTable(
                name: "WebsiteServices");

            migrationBuilder.DropIndex(
                name: "IX_Websites_WebsiteServiceId",
                table: "Websites");

            migrationBuilder.DropIndex(
                name: "IX_ServiceGroups_WebsiteServiceId",
                table: "ServiceGroups");

            migrationBuilder.DropColumn(
                name: "WebsiteServiceId",
                table: "Websites");

            migrationBuilder.DropColumn(
                name: "WebsiteServiceId",
                table: "ServiceGroups");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "WebsiteServiceId",
                table: "Websites",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WebsiteServiceId",
                table: "ServiceGroups",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "WebsiteServices",
                columns: table => new
                {
                    WebsiteServiceId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IpAddresses = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ServiceIpAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ServicePort = table.Column<int>(type: "int", nullable: false),
                    SiteDirectory = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    WebRootDirectory = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebsiteServices", x => x.WebsiteServiceId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Websites_WebsiteServiceId",
                table: "Websites",
                column: "WebsiteServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceGroups_WebsiteServiceId",
                table: "ServiceGroups",
                column: "WebsiteServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceGroups_WebsiteServices_WebsiteServiceId",
                table: "ServiceGroups",
                column: "WebsiteServiceId",
                principalTable: "WebsiteServices",
                principalColumn: "WebsiteServiceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Websites_WebsiteServices_WebsiteServiceId",
                table: "Websites",
                column: "WebsiteServiceId",
                principalTable: "WebsiteServices",
                principalColumn: "WebsiteServiceId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
