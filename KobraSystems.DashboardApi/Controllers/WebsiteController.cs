﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Db;
using KobraSystems.DashboardApi.Services;

namespace KobraSystems.DashboardApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class WebsiteController : ControllerBase
    {
        private readonly DashboardApiContext _db;
        private readonly WebsiteCreationService _websiteCreationService;
        private readonly Services.Db.WebsiteService _websiteService;

        public WebsiteController(DashboardApiContext context, WebsiteCreationService websiteCreationService, Services.Db.WebsiteService websiteService)
        {
            _db = context;
            _websiteCreationService = websiteCreationService;
            _websiteService = websiteService;
        }

        [HttpPost]
        public async Task<IActionResult> SetupWebsites()
        {
            var websitesThatNeedSetup = _db.Websites.Where(w => w.Status == Website.WebsiteStatus.Pending_Setup);
            foreach (var website in websitesThatNeedSetup)
            {
                var websiteService = website.Product.ServiceGroup.WebsiteService;

                await _websiteCreationService.CreateWebsiteAsync(websiteService, website);
                await _websiteService.UpdateWebsiteAsync(website.WebsiteId, website.Name, Website.WebsiteStatus.Active);
            }

            return Ok();
        }
    }
}
