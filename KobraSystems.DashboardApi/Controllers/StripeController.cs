﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Configuration;
using KobraSystems.DashboardApi.Db;
using KobraSystems.DashboardApi.Services;
using KobraSystems.DashboardApi.Services.Stripe;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Stripe;
using Stripe.Checkout;
using Account = KobraSystems.DashboardApi.Db.Account;
using AccountService = KobraSystems.DashboardApi.Services.Db.AccountService;
using ProductService = KobraSystems.DashboardApi.Services.Db.ProductService;

namespace KobraSystems.DashboardApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class StripeController : ControllerBase
    {
        private readonly StripeOptions _stripeOptions;
        private readonly AuthService _authService;
        private readonly AccountService _accountService;
        private readonly ProductService _productService;
        private readonly DashboardApiContext _db;
        private readonly StripeCustomerService _customerService;
        private readonly StripeCheckoutService _checkoutService;

        public StripeController(DashboardApiContext context, IOptions<StripeOptions> stripeOptions, AuthService authService, AccountService accountService, ProductService productService, StripeCustomerService customerService, StripeCheckoutService checkoutService)
        {
            _db = context;
            _stripeOptions = stripeOptions.Value;
            _authService = authService;
            _accountService = accountService;
            _productService = productService;
            _customerService = customerService;
            _checkoutService = checkoutService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        public IActionResult GetPublicKeys()
        {
            return Ok(_stripeOptions.PublishableKey);
        }


        [Authorize]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CheckoutResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Checkout([FromBody] CheckoutRequest model)
        {
            int accountId = _authService.GetAccountIdFromAuthHeaders(Request);
            Account? account = await _accountService.GetAccountByIdAsync(accountId);

            if (account == null)
            {
                return NotFound();
            }

            var product = await _productService.GetProductById(model.ProductId);

            if (product == null)
            {
                return NotFound();
            }

            Customer customer = _customerService.CreateCustomer(account.EmailAddress);

            account.StripeCustomerId = customer.Id;
            await _db.SaveChangesAsync();

            Session session = _checkoutService.CreateCheckoutSession(account.AccountId, account.EmailAddress, customer.Id, model.WebSiteName, product);

            var response = new CheckoutResponse
            {
                SessionId = session.Id,
                PublicKey = _stripeOptions.PublishableKey
            };

            return Ok(response);
        }

        public class CheckoutRequest
        {
            public int ProductId { get; set; }
            public string WebSiteName { get; set; } = string.Empty;
        }

        public class CheckoutResponse
        {
            public string SessionId { get; set; } = string.Empty;
            public string PublicKey { get; set; } = string.Empty;
        }
    }
}
