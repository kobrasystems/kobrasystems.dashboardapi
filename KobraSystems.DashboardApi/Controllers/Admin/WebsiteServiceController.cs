﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Db;
using Microsoft.EntityFrameworkCore;

namespace KobraSystems.DashboardApi.Controllers.Admin
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    public class WebsiteServiceController : ControllerBase
    {
        private readonly DashboardApiContext _db;

        public WebsiteServiceController(DashboardApiContext context)
        {
            _db = context;
        }

        // This is commented out until admin tokens are issued
        //[HttpPost]
        //public async Task<IActionResult> CreateWebsiteService([FromBody] CreateWebsiteServiceRequest model)
        //{
        //    _db.WebsiteServices.Add(new WebsiteService
        //    {
        //        Name = model.Name,
        //        Auth = model.Auth,
        //        IpAddresses = model.IpAddresses,
        //        ServiceIpAddress = model.ServiceIpAddress,
        //        ServicePort = model.ServicePort,
        //        SiteDirectory = model.SiteDirectory,
        //        WebRootDirectory = model.WebRootDirectory,
        //    });

        //    await _db.SaveChangesAsync();

        //    return Ok();
        //}
    }

    public class GetWebsiteServiceRequest
    {
        public int WebsiteServiceId { get; set; }
    }

    public class CreateWebsiteServiceRequest
    {
        [Required]
        public string Name { get; set; } = string.Empty;
        [Required]
        public string SiteDirectory { get; set; } = string.Empty;
        [Required]
        public string WebRootDirectory { get; set; } = string.Empty;
        [Required]
        public string ServiceIpAddress { get; set; } = string.Empty;
        [Required]
        public string IpAddresses { get; set; } = string.Empty;
        [Range(1, 65535)]
        public int ServicePort { get; set; }
        [Required]
        public string Auth { get; set; } = string.Empty;
    }
}
