﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Configuration;
using KobraSystems.DashboardApi.Db;
using KobraSystems.DashboardApi.Services.Stripe;
using Microsoft.Extensions.Options;
using Stripe;
using Stripe.Checkout;

namespace KobraSystems.DashboardApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class StripeWebhookController : ControllerBase
    {
        private readonly IOptions<StripeOptions> _stripeOptions;
        private readonly StripeCheckoutService _stripeCheckoutService;
        private readonly DashboardApiContext _db;

        public StripeWebhookController(DashboardApiContext context, IOptions<StripeOptions> stripeOptions, StripeCheckoutService stripeCheckoutService)
        {
            _db = context;
            _stripeOptions = stripeOptions;
            _stripeCheckoutService = stripeCheckoutService;
        }

        [HttpPost]
        public async Task<IActionResult> CheckoutCompleted()
        {
            string json = await new StreamReader(HttpContext.Request.Body).ReadToEndAsync();

            try
            {
                var stripeEvent = EventUtility.ConstructEvent(json, Request.Headers["Stripe-Signature"],
                    _stripeOptions.Value.WebhookSecret);

                switch (stripeEvent.Type)
                {
                    case Events.CheckoutSessionCompleted:

                        if (stripeEvent.Data.Object is Session session)
                        {
                            var sessionId = session.Id;

                            if (session.Metadata.TryGetValue("AccountId", out string? accountIdAsStr) && string.IsNullOrWhiteSpace(accountIdAsStr) == false)
                            {
                                if (session.Metadata.TryGetValue("Name", out string? webSiteName) && string.IsNullOrWhiteSpace(webSiteName) == false)
                                {
                                    if (int.TryParse(accountIdAsStr, out int accountId))
                                    {
                                        var account = _db.Accounts.SingleOrDefault(a => a.AccountId == accountId);

                                        if (account != null)
                                        {
                                            var lineItems = _stripeCheckoutService.GetSessionLineItems(sessionId);

                                            foreach (var lineItem in lineItems.Data)
                                            {
                                                var orderedProduct = _db.Products.SingleOrDefault(p => p.StripeProductId == lineItem.Price.ProductId);

                                                if (orderedProduct != null)
                                                {
                                                    account.WebSites.Add(new Website
                                                    {
                                                        Name = webSiteName,
                                                        IisSiteName = webSiteName,
                                                        Status = Website.WebsiteStatus.Pending_Setup,
                                                        Product = orderedProduct,
                                                    });

                                                    await _db.SaveChangesAsync();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        break;
                }
            }
            catch (StripeException ex)
            {
                await System.IO.File.WriteAllTextAsync(_stripeOptions.Value.StripeLogFile, ex.ToString());
                return BadRequest();
            }
            catch (Exception ex)
            {
                await System.IO.File.WriteAllTextAsync(_stripeOptions.Value.StripeLogFile, ex.ToString());
                return BadRequest();
            }

            return Ok();
        }
    }
}
