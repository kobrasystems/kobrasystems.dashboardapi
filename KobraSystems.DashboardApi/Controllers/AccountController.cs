﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Db;
using KobraSystems.DashboardApi.Services;
using KobraSystems.DashboardApi.Services.Db;

namespace KobraSystems.DashboardApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly DashboardApiContext _db;
        private readonly AuthService _authService;
        private readonly AccountService _accountService;

        public AccountController(DashboardApiContext context, AuthService authService, AccountService accountService)
        {
            _db = context;
            _authService = authService;
            _accountService = accountService;
        }

        [HttpPost("Signup")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SignupResult))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Signup(SignupRequest model)
        {
            if (_db.Accounts.Any(a => a.EmailAddress == model.EmailAddress))
            {
                return BadRequest();
            }

            if (AuthService.PasswordStrengthChecker.PasswordCheck.GetPasswordStrength(model.Password) <=
                AuthService.PasswordStrengthChecker.PasswordStrength.Strong)
            {
                return BadRequest();
            }

            var passwordDigest = BCrypt.Net.BCrypt.HashPassword(model.Password);

            var account = await _accountService.CreateAccountAsync(model.FirstName, model.LastName, model.CompanyName, model.Address1,
                model.Address2, model.City, model.CountryCode, model.EmailAddress, model.PhoneNumber, passwordDigest,
                model.PostalCode, model.State);

            var authToken = _authService.CreateToken(account.AccountId);

            return Ok(new SignupResult
            {
                AuthToken = authToken
            });
        }

        [HttpPost("Login")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LoginResult))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult Login([FromBody] LoginRequest model)
        {
            var account = _db.Accounts.SingleOrDefault(a => a.EmailAddress == model.EmailAddress);
            if (account == null)
            {
                return Forbid();
            }

            if (BCrypt.Net.BCrypt.Verify(model.Password, account.PasswordDigest) == false)
            {
                return Forbid();
            }

            var authToken = _authService.CreateToken(account.AccountId);

            return Ok(new LoginResult
            {
                AuthToken = authToken
            });
        }
    }

    public class LoginResult
    {
        public string AuthToken { get; set; } = string.Empty;
    }

    public class LoginRequest
    {
        [EmailAddress]
        public string EmailAddress { get; set; } = string.Empty;

        [Required]
        public string Password { get; set; } = string.Empty;
    }

    public class SignupResult
    {
        public string AuthToken { get; set; } = string.Empty;
    }

    public class SignupRequest
    {
        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; } = string.Empty;

        [Required]
        public string Password { get; set; } = string.Empty;

        [Required]
        public string FirstName { get; set; } = string.Empty;

        [Required]
        public string LastName { get; set; } = string.Empty;

        [Required]
        public string Address1 { get; set; } = string.Empty;


        public string Address2 { get; set; } = string.Empty;

        [Required]
        public string City { get; set; } = string.Empty;

        [Required]
        public string State { get; set; } = string.Empty;

        [Required]
        public string CountryCode { get; set; } = string.Empty;

        [Required]
        public string PhoneNumber { get; set; } = string.Empty;

        [Required]
        public string PostalCode { get; set; } = string.Empty;

        public string CompanyName { get; set; } = string.Empty;
    }
}
