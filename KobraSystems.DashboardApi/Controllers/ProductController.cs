﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KobraSystems.DashboardApi.Db;
using KobraSystems.DashboardApi.Services.Db;

namespace KobraSystems.DashboardApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly DashboardApiContext _db;
        private readonly ProductService _productService;

        public ProductController(DashboardApiContext context, ProductService productService)
        {
            _db = context;
            _productService = productService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ProductWrapper[]), StatusCodes.Status200OK)]
        public IActionResult Get()
        {
            var products = _productService.GetProducts().Select(p => new ProductWrapper(p)).ToArray();

            return Ok(products);
        }

        public class ProductWrapper
        {
            public ProductWrapper(Product product)
            {
                ProductId = product.ProductId;
                AnnualCost = product.AnnualCost;
                MonthlyCost = product.MonthlyCost;
                BandwidthInMb = product.BandwidthInMb;
                DiskLimitInMb = product.DiskLimitInMb;
                DomainAliasLimit = product.DomainAliasLimit;
                DomainLimit = product.DomainLimit;
                EmailAccountLimit = product.EmailAccountLimit;
                EmailDiskLimitInMb = product.EmailDiskLimitInMb;
                FtpAccountLimit = product.FtpAccountLimit;
                MySqlDiskLimitInMb = product.MySqlDiskLimitInMb;
                Name = product.Name;
                Description = product.Description;
                ShownToCustomer = product.ShownToCustomer;
                SqlServerDiskLimitInMb = product.SqlServerDiskLimitInMb;
                SubDomainsLimit = product.SubDomainsLimit;
                StripePriceId = product.StripePriceId;
                StripeProductId = product.StripeProductId;
            }

            public long AnnualCost { get; set; }

            public string Description { get; set; }

            public string StripeProductId { get; set; }

            public string StripePriceId { get; set; }

            public int SubDomainsLimit { get; set; }

            public int SqlServerDiskLimitInMb { get; set; }

            public bool ShownToCustomer { get; set; }

            public string Name { get; set; }

            public int MySqlDiskLimitInMb { get; set; }

            public int FtpAccountLimit { get; set; }

            public int EmailDiskLimitInMb { get; set; }

            public int EmailAccountLimit { get; set; }

            public int DomainLimit { get; set; }

            public int DomainAliasLimit { get; set; }

            public int DiskLimitInMb { get; set; }

            public int BandwidthInMb { get; set; }

            public long MonthlyCost { get; set; }

            public int ProductId { get; set; }
        }
    }
}
